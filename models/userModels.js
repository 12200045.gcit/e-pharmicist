const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt=require("bcryptjs")

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Please tell us your name!'],
    },
    email: {
        type: String,
        required: [true, 'Please provide your email'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, 'Please provide a valid email'],
    },
    photo: {
        type: String,
        default: 'default.jpg',
    },
    role:{
        type: String,
        enum: ['user', 'sme', 'pharmacist', 'admin'],
        default: 'user',
    },

    password: {
        type: String,
        required: [true, 'Please provide a password!'],
        minlength: 8,
        //password wont be included when we get the users
        select: false,
    },

    passwordConfirm: {
        type:String,
        required:[true, 'Please confirm your password'],
        validate:{
            validator:function(e1){
                return e1 === this.password
            },
            message: "password are not the same",
        }
    },

    active: {
        type: Boolean,
        default: true,
        select: false,
    },
})

// userSchema.pre('save' } is middle ware for mongoose
userSchema.pre('save', async function (next){

    //only run this function if password was actually modified
    if(!this.isModified('password')) return next()

    //hash the password with cost of 12. 12 is number of time to encrypt the password.
    this.password = await bcrypt.hash(this.password, 12)

    //delete passwordconfirm field
    this.passwordConfirm = undefined
    next()

})

userSchema.methods.correctPassword = async function (
    candidatePassword,
    userPassword,
) {
    return await bcrypt.compare(candidatePassword, userPassword)
}

const User = mongoose.model('User', userSchema)
module.exports = User